section .data
    numbers: db '0123456789'    

section .text
 
;глобал всех функций
GLOBAL exit
GLOBAL string_length
GLOBAL print_string
GLOBAL print_char
GLOBAL print_newline
GLOBAL print_uint
GLOBAL print_int
GLOBAL read_char
GLOBAL read_word
GLOBAL string_equals
GLOBAL parse_uint
GLOBAL parse_int
GLOBAL string_copy
GLOBAL print_error


; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, 60
    xor rdi, rdi
	syscall
    ret 

; Принимает указатель rdi на нуль-терминированную строку, возвращает её длину в rax
string_length:
    xor rax, rax
    .looop:
            
    .count:
        cmp byte [rdi+rax], 0
        je .end_loop
        inc rax
        jmp .count
    .end_loop:
        ret

; Принимает указатель rdi на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax ; Длина строки
    mov rax, 1 ;вывод
    mov rsi, rdi ; начало строки
    mov rdi, 1 ; такая настройка
    syscall
    ret

; Принимает указатель rdi на нуль-терминированную строку, выводит её в stderr
print_error:
        push rdi
		call string_length
        pop rsi
		mov rdx, rax
		mov rax, 1
		mov rdi, 2
		syscall
        ret

; Принимает код символа rdi и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp ; символ хранится в стеке
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате (изначально хранится в rdi)
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    push rdi
    push rbp
    mov rbp, rsp
    mov rcx, 10
    push 0

    .uint_loop:
        xor rdx, rdx
        div rcx
        dec rsp
        mov rdx, [rdx + numbers]
        mov [rsp], dl
        cmp rax, 0
        jne .uint_loop

    mov rdi, rsp
    call print_string
    mov rsp, rbp
    pop rbp
    pop rdi
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jnl end_print_int
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi

    end_print_int:
        jmp print_uint

; Принимает два указателя (rdi и rsi) на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx


    .equals_loop:
        mov byte al, [rdi + rcx]
        mov byte ah, [rsi + rcx]
        cmp al, ah
        jne .no_equals

        inc rcx
        cmp al, 0
        jne .equals_loop

        mov rax, 1
        ret


    .no_equals:
        mov rax, 0
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rsi, rsp
    mov rdx, 1
    mov rax, 0
    mov rdi, 0
    syscall

    pop rax
    cmp al, 0xA
    jne .good_read
    xor rax, rax

    .good_read:
        ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rbx
    mov r8, rdi
    mov r9, rsi

    .loop_first:
    push r9
    call read_char
    pop r9
    cmp al, 0x20
    je .loop_first
    cmp al, 0x9
    je .loop_first
    cmp rax, 0
    je .end_word
    mov rbx, 1
    mov [r8], rax


    .loop_next:
    push r9
    call read_char
    pop r9
    cmp rax, 0
    je .end_word
    cmp al, 0x20
    je .end_word
    cmp al, 0x9
    je .end_word
    mov [r8+rbx], rax
    inc rbx 
    cmp rbx, r9
    jl .loop_next

    mov rax, 0
    pop rbx
    ret

    .end_word:
    mov rdx, rbx
    lea rax, [r8]
    pop rbx
    ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rcx
    push rbx
    mov r8, 10
    xor rax, rax
    xor rcx, rcx
    jmp .read_number

    .more_0:
        cmp bl, '9'
        jg .finish_parse_uint

    .save_parse_number:
        mul r8
        sub bl, '0'
        add al, bl
        inc rcx

    .read_number:
        mov bl, byte[rdi+rcx]
        cmp bl, '0'
        jge .more_0

    .finish_parse_uint:
        mov rdx, rcx
        pop rbx
        pop rcx
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push rdi
    mov al, byte[rdi]
    cmp al, '-'
    jne .next_step_parse_int
    inc rdi
    .next_step_parse_int:
        call parse_uint
        cmp dl, 0
        je .finish_parse_int
        cmp rdi, [rsp]
        je .finish_parse_int
        neg rax
        inc rdx

    .finish_parse_int:
        pop rdi
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    
    push rcx
    je .fail_string_copy
    call string_length
    inc rax
    xor rcx, rcx
    cmp rax, rdx
    jg .fail_string_copy
    xor r8, r8
    .loop_string_copy:
        cmp rcx, rax
        je .succes_string_copy
        mov r8, [rdi + rcx]
        mov [rsi+rcx], r8
        inc rcx
        jmp .loop_string_copy

    .succes_string_copy:
        pop rcx
        ret

    .fail_string_copy:
        pop rcx
        xor rax, rax
        ret

