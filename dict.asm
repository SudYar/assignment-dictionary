extern string_equals

section .text

GLOBAL find_word

find_word:
	.loop_find_word:
		test rsi, rsi  ; проверка не дошли ли мы до конца словаря
		je .finish_search
		push rdi
		push rsi
		add rsi, 0x8
		call string_equals
		pop rsi
		pop rdi
		cmp al, 0x1
		je .finish_search
		mov rsi, [rsi]  ; берем следующий указатель
		jmp .loop_find_word

	.finish_search:
		mov rax, rsi  ; если мы не нашли слово, то в rsi должен лежать 0
		ret
