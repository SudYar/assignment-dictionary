%include "words.inc"
%include "lib.inc"


GLOBAL _start

section .rodata

not_found: db "Такой ключ не найден", 0
buff_err: db "Вы ввели строку больше 255",0

section .text

_start:
	sub rsp, 255
	mov rdi, rsp
	mov rsi, 255
	call read_word
	cmp rax, 0
	je .fail_buff
	mov rdi, rsp
	mov rsi, next
	call find_word
	add rsp, 255
	cmp rax, 0
	je .fail_found
	jmp .success_found
	


	.success_found:
		add rax, 8
		mov rdi, rax
		push rax
		call string_length
		pop rdi
		add rdi, rax
		inc rdi
		call print_string
		call print_newline
		call exit

	.fail_found:
		mov rdi, not_found
		jmp .print_error

	.fail_buff:
		mov rdi, buff_err
		jmp .print_error

	.print_error:
		call print_error
		call print_newline
		call exit
